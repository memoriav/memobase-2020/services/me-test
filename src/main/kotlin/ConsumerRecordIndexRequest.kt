/*
 * elasticsearch consumer service
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.memobase

import org.apache.kafka.clients.consumer.ConsumerRecord
import org.elasticsearch.action.index.IndexRequest
import org.elasticsearch.common.xcontent.XContentType

class ConsumerRecordIndexRequest(record: ConsumerRecord<String, String>, index: String) : IndexRequest(index) {
    private val consumerRecord = record
    val value: String? = consumerRecord.value()
    init {
        if (record.key() != null && record.key() != "") {
            this.id(record.key())
        }
        if (value != null && value != "{}") {
            this.source(value, XContentType.JSON)
        } else {
            throw NullPointerException("No data inside of the message {}!")
        }
    }
}