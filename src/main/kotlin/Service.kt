/*
 * Table Data Import Service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.memobase

import org.apache.logging.log4j.LogManager
import org.memobase.settings.SettingsLoader

class Service(file: String = "app.yml") {
    private val log = LogManager.getLogger("ElasticBulkActionService")

    val settings = SettingsLoader(
            listOf(
                    "elastic.index",
                    "elastic.port",
                    "elastic.host"
            ),
            file,
            useConsumerConfig = true
    )

    private val consumer = Consumer(settings.inputTopic, settings.kafkaConsumerSettings)
    private val processor = BulkProcessor(consumer, settings.appSettings, log)

    fun run() {
        while (true) {
            processor.process()
        }
    }
}

