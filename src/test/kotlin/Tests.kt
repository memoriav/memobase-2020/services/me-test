/*
 * sftp-reader
 * Copyright (C) 2019  Memobase
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.memobase

import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.common.serialization.StringSerializer
import org.apache.logging.log4j.LogManager
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.memobase.testing.EmbeddedKafkaExtension
import java.util.Properties

@ExtendWith(EmbeddedKafkaExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class Tests {
    private val log = LogManager.getLogger("LocalTestsLogger")

    private val producer: KafkaProducer<String, String>

    init {
        val props = Properties()
        props.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:12345")
        props.setProperty(ProducerConfig.CLIENT_ID_CONFIG, "test-group-1")
        props.setProperty(ProducerConfig.COMPRESSION_TYPE_CONFIG, "zstd")
        props.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer::class.qualifiedName)
        props.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer::class.qualifiedName)
        producer = KafkaProducer(props)
    }

    @Test
    fun `test processor`() {
        /*
        val service = Service("app.yml")
        log.info("Created Service")
        producer.send(ProducerRecord(service.settings.inputTopic, "https://memobase.ch/record/IFRC-No_ID_053", "{\"id\":\"https://memobase.ch/record/IFRC-No_ID_053\",\"abstract\":{\"fr\":\"Le film retrace la visite de Henrik Beer, Secétaire general de la Ligue, en Arabie Saoudite. Le film montre son arrivée, les visites qu'il effectue ainsi que les réunions avec les membres de la Croix-Rouge du pays\"},\"dateCreated.date\":\"1968\",\"dateCreated.facet\":[\"0~20.Jahrhundert~\",\"1~20.Jahrhundert~1961-1970#\"],\"dateIssued.date\":\"1968\",\"dateIssued.facet\":[\"0~20.Jahrhundert~\",\"1~20.Jahrhundert~1961-1970#\"],\"placeRelatedRaw\":{\"fr\":\"Arabie Saoudite\"},\"descriptiveNote\":{\"fr\":\"Pas sur YouTube\"},\"title\":\"Saudi Arabia\",\"sourceID\":\"No_ID_053\",\"AgentcreatorRaw.relation\":\"Autor\",\"AgentcreatorRaw.name\":{\"fr\":\"[aucune information]\"},\"rightsHolder\":{\"fr\":\"La Fédération internationale des Sociétés de la Croix-Rouge et du Croissant-Rouge\"},\"scopeAndContent\":{\"fr\":\"projets Memoriav : 2015\"},\"type.keyword\":\"Film\"}"))
        log.info("Wrote Message")
        service.run()
        */
        assert(true)
    }
}
