## Elastic Bulk Action Service

A service which can create, index, update and delete documents from a elasticsearch cluster with a bulk processor. 
The source for the consumer is one or several Kafka topics. Failed operations are reported to a specific topic.